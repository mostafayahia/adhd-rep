﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using UnityEngine;

/// <summary>
/// XML manager to manage Save and load data whatever we want , we cann Add more items to the Dilaouge in Class <Dilaouge_item > 
/// how this script work ? ! 
///  it depends on the scene we create any number of inexes from <Dialouge_item>class , we add string of text , string of audio clips path  , boolean to check if this text need animation to play or not 
/// </summary>
public partial  class Xml_Manager : MonoBehaviour
{

    // singleton pattern 
    public static Xml_Manager ins;
    public Lesson.ItemDatabase itemDB;
    private void Awake()
    {
        ins = this;
        SaveItems();
    }

    public void SaveItems()
    {
        XmlSerializer serilizer = new XmlSerializer(typeof(Lesson.ItemDatabase));
        FileStream stream = new FileStream(Application.dataPath + "/StreamingAssets/XML/ADHD_Demo.xml", FileMode.Create);
        serilizer.Serialize(stream, itemDB);
        stream.Close();

    }
    public void Loaditems()
    {
        XmlSerializer serilizer = new XmlSerializer(typeof(Lesson.ItemDatabase));
        FileStream stream = new FileStream(Application.dataPath + "/StreamingAssets/XML/ADHD_Demo.xml", FileMode.Open);
        itemDB = serilizer.Deserialize(stream) as Lesson.ItemDatabase;
        stream.Close();
    }

}
          [System.Serializable]
public class Lesson
{
    [System.Serializable]
    public class ItemDatabase
    {
        public List<Dialouge_item> Dialouge_items = new List<Dialouge_item>();

    }
    [System.Serializable]
    public struct Animation_manager
    {
        public bool Animation_exist;
        public bool Animation_Flag;
        public Animations_type Animation_index; // here i can choose the text animation from the list of animations i have 


    }
    [System.Serializable]
    public struct Audio_manager
    {
        public bool Audio_exist;
        public string audioclips;
        public Audio_type audio_index;

    }
    [System.Serializable]
    public struct Text_manager
    {
        public string Text_Dialouge;
        public Text_index Position_index; // here i can give specific string to Display on specifit list of text already exist in the Scene 

    }
    [System.Serializable]
    public struct Image_manager
    {
        public bool Image_Exist;// i can use this bool to check if the text will contain image with it or no 
        public Images_position position_image;// here i can select which image i need to display with a specific text 
    }
}
[System.Serializable]
public class Dialouge_item
{
 
    public Lesson.Text_manager manage_text; 
    public Lesson.Animation_manager manage_animation;
    public Lesson.Audio_manager manage_audio;
    public Lesson.Image_manager manage_image; 
}


