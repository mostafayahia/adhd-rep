﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI; 
/// <summary>
/// This Script to manage the Scenario of the Scene 
/// </summary>
public partial  class Scene_Manager : MonoBehaviour //  i am using partial so everyone can create his/her own class with different implementation 
{
    #region XML AREA 
    [Header("Loading Data From XML")]
    public List<Text> Display_txts;
    int xml_txt_position_index; 
  
    /// <summary>
    /// this method will occur depending on specific Event 
    /// </summary>
    IEnumerator LoadfromXML ()
    {
        // loop on XML file 
        for (int i = 0; i < Xml_Manager.ins.itemDB.Dialouge_items.Count; i++)
        {

            xml_txt_position_index =  (int)     Xml_Manager.ins.itemDB.Dialouge_items[i].manage_text.Position_index; // pass the position of Text index to Temp_variable 
            Display_txts[xml_txt_position_index].GetComponent<ArabicText>().Text = Xml_Manager.ins.itemDB.Dialouge_items[i].manage_text.Text_Dialouge; // pass the xml text in the position of list of texts 

          

            yield return new WaitForSeconds(2);
   
        
          //  Display_txts[xml_txt_position_index].text = Xml_Manager.ins.itemDB.Dialouge_items[i].manage_text.Text_Dialouge; // pass the xml text in the position of list of texts 

            Animators();

            Display_txts[xml_txt_position_index].GetComponent<Animator>().enabled = true;
       
            Display_txts[xml_txt_position_index].enabled = true;


            if (Xml_Manager.ins.itemDB.Dialouge_items[i].manage_image.Image_Exist)// here i am checking , if this text has image to show or not 
            {
                lis_image[xml_txt_position_index].enabled = true; // Enable the text image 
                lis_image[xml_txt_position_index].sprite = list_sprite[(int)Xml_Manager.ins.itemDB.Dialouge_items[i].manage_image.position_image]; // Give the index of sprite  to specific image 
            }
            anim = Display_txts[xml_txt_position_index].GetComponent<Animator>();
            animatoroverrride = new AnimatorOverrideController(anim.runtimeAnimatorController);
            anim.runtimeAnimatorController = animatoroverrride;
            animatoroverrride["txt0"] = Text_animation_clips[(int)Xml_Manager.ins.itemDB.Dialouge_items[i].manage_animation.Animation_index];

            if (Xml_Manager.ins.itemDB.Dialouge_items[i].manage_audio.Audio_exist)
            {
                audiosource.clip = audioClips[i];
                audiosource.Play(); 
            }

                                                                  


        }
    }

    #endregion

    #region Animation Area 
    [Header("Animation Area")]
    public Animator anim;
   public AnimationClip[] Text_animation_clips;
    AnimatorOverrideController animatoroverrride;
    public int animation_int; 
    private void Start()
    {
     StartCoroutine(   LoadfromXML()); 
       // anim = GetComponent<Animator>(); 
      
    }
    //public void Test_btn(int value)
    //{
    //    switch (value)
    //    {
    //        case 0:
    //            //  anim.SetInteger("txt_index", value);
    //            Animators(); 

    //            Display_txts[0].GetComponent<Animator>().enabled = true;
    //            anim = Display_txts[0].GetComponent<Animator>();
    //            animatoroverrride = new AnimatorOverrideController(anim.runtimeAnimatorController);
    //            anim.runtimeAnimatorController = animatoroverrride;
    //            animatoroverrride["txt0"] = Text_animation_clips[animation_int]; 

    //            break;

    //        case 1:
    //            Animators();

    //            Display_txts[1].GetComponent<Animator>().enabled = true;
    //            anim = Display_txts[1].GetComponent<Animator>();
    //            animatoroverrride = new AnimatorOverrideController(anim.runtimeAnimatorController);
    //            anim.runtimeAnimatorController = animatoroverrride;
    //            animatoroverrride["txt0"] = Text_animation_clips[animation_int];
    //            break;

    //        case 2:

    //            anim.SetInteger("txt_index", value);
    //            animatoroverrride["txt0"] = Text_animation_clips[2];
    //            break;
    //    }
    //}
    private void Animators()
    {
        for (int i = 0; i < Display_txts.Count; i++)
        {
        Display_txts[i].GetComponent<Animator>().enabled = false;
        }
    }

    #endregion


    #region Images Area 
    [Header ("Images Area")]
    public List<Sprite> list_sprite;
    public List<Image> lis_image;
    #endregion

    #region Audio Area 

    [Header("Audio Area")]
    public List<AudioClip> audioClips;
    public AudioSource audiosource; 
   
    #endregion
}
