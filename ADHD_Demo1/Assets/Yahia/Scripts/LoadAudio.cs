﻿using UnityEngine;
using System.Collections.Generic; 
using System.IO;
using UnityEngine.Video;
using UnityEngine.Networking ; 
    using System.Collections;

/// <summary>
/// This Script target is to get the Audio from Streaming Assets , to load inside the scene
/// </summary>
public class LoadAudio : MonoBehaviour
{
    public List<string> audioname ;// i will try to get it is content from XML File 

    [Header("Audio Stuff")]
    public AudioSource audiosource;
    public AudioClip audioclip;
    public string soundpath;
    public List<AudioClip> Testing_Audio; 
    private void Awake()
    {
       // audiosource = gameObject.AddComponent<AudioSource>();// Add component to the game object 
        soundpath = "file://" + Application.streamingAssetsPath + "/Sound/";//file streaming path 
       
        StartCoroutine(LoadAudio_couroutine());

        
    }
    
    private void Start()
    {
       
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.streamingAssetsPath + "/Sound/");
        //     print("Streaming Assets Path: " + Application.streamingAssetsPath);
        string path2;
        FileInfo[] allFiles = directoryInfo.GetFiles("*.*");
        List<string> directories = new List<string>(); 
        foreach (var file in allFiles)
        {
           
            path2 = file.DirectoryName + file.Name;
            directories.Add(path2); 
         
        }
        for (int i = 0; i < Xml_Manager.ins.itemDB.Dialouge_items.Count; i+=2)
        {
           
            Xml_Manager.ins.itemDB.Dialouge_items[i].manage_audio.audioclips = directories[i];
         

        }
       
    }
    private void Update()
    {
       
    }
    [Header("Audio list")]
 public   Scene_Manager scenemana_obj; 
    private IEnumerator LoadAudio_couroutine ()
    {
        DirectoryInfo directoryInfo = new DirectoryInfo(Application.streamingAssetsPath + "/Sound/");// i am using the Directory to ge the of <streaming assets> and Access <Sound> folder that is inside Streaming Assets Folder
        FileInfo[] allFiles = directoryInfo.GetFiles("*.*");// i am using file info to get All Files inside the <sound> folder 
        print("COUNT=" + allFiles.Length); 
        for (int i = 0; i < allFiles.Length; i++) // For loop on all files in the <Sound> Folder 
        {
            if (i%2==0) // Cause there is meta files i used this equation to get only the audio clip files 
            {
           
                audioname.Add(  allFiles[i].Name); // Here i can Add the Audio clip in the list by name 
                
            }


        }
        for (int i = 0; i < audioname.Count; i++)
        {
            print("i = " + i);
            WWW request = getaudiofromfile(soundpath, audioname[i]);// pass the path of the clip 
            yield return request;
            audioclip = request.GetAudioClip();
            scenemana_obj.audioClips.Add(request.GetAudioClip());
        }
       
            
        }
       

    
   
    private void  PlayAudioFile ()
    {
        audiosource.clip = audioclip;
        audiosource.Play();
     //   audiosource.loop = true; 
    }
    /// <summary>
    /// WWW property to get the request path with file name , here i am sending a file name i am sure it is audio clip to retreive it is all path 
    /// </summary>
    /// <param name="path"></param>
    /// <param name="filename"></param>
    /// <returns></returns>
    private WWW getaudiofromfile (string path , string filename)
    {

  //      print("the file " + filename ); 
        string audiotoload = string.Format(path + "{0}", filename);
        WWW request = new WWW(audiotoload);
        return request; 
    }

  
}
