﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Temp. script.
/// </summary>
public class AnimationBoardController : MonoBehaviour
{
    public GameObject BoardMovingBackground;

    bool boolBackground = false;

    Animator animator;
    Renderer renderer;

    void Start()
    {
        animator = BoardMovingBackground.GetComponent<Animator>();
        renderer = BoardMovingBackground.GetComponent<Renderer>();
        print(renderer.material.color);
    }

    private void Update()
    {
        if (Input.GetKeyDown("a")) boolBackground = !boolBackground;

        if (boolBackground)
        {
            BoardMovingBackground.active = true;
        }
        else
        {
            animator.SetBool("Appear", false);
        }

        //if(renderer.material.color.a == 0f)
        //{
        //    Debug.Log("Hakona Matata ");
        //    BoardMovingBackground.active = false;

        //}
    }

    void DeactivateBoardMovingBackground()
    {
        BoardMovingBackground.active = false;
    }
}

