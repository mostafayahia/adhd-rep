﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoardMovingBackground : MonoBehaviour
{
    public float scrollX;
    public float scrollY;

    bool show = false;
    bool boolBackground = false;

    private void OnEnable()
    {
        StartCoroutine(setshow());
    }
    IEnumerator setshow()
    {
        yield return new WaitForSeconds(0.05f);
        show = true;
    }

    private void OnDisable()
    {
        show = false;
    }

    void Update()
    {
        float offsetX = Time.time * scrollX;
        float offsetY = Time.time * scrollY;

        if (Input.GetKeyDown("x")) boolBackground = !boolBackground;

        if (boolBackground)
        {
            GetComponent<Renderer>().material.mainTextureOffset = new Vector2(offsetX, offsetY);
        }
    }

    public void AnimationDone()
    {
        if (!show)
            return;

        gameObject.SetActive(false);
    }


}
